from db import db

class Estudiante(db.Model):
    __tablename__="estudinate"
    
    id=db.Column(db.Integer, primary_key=True)
    nombre=db.Column(db.String(50))
    codigo=db.Column(db.String(70))
    email=db.Column(db.String(15))
    
    def __init__(self, nombre, email, codigo):
        self.nombre=nombre
        self.codigo=codigo
        self.email=email